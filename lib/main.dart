import 'package:window_size/window_size.dart' as WindowSize;
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

import 'app.dart';

Future<void> main() async {
  await DotEnv.load(fileName: ".env");

  print('Run app...');

  WindowSize.setWindowTitle('STAMP');
  WindowSize.setWindowMaxSize(Size(525.0, 900.0));
  WindowSize.setWindowMinSize(Size(525.0, 900.0));

  runApp(MyApp());

  print('App runned');
}

