import 'package:dio/dio.dart';
import 'package:stampapp/configs/config.dart';

// or new Dio with a BaseOptions instance.
var options = BaseOptions(
  baseUrl: configs['api'],
  connectTimeout: 5000,
  receiveTimeout: 3000,
  contentType: 'application/json; charset=utf-8'
);
Dio dio = Dio(options);