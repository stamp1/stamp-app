import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:stampapp/api/api.dart';

import 'package:stampapp/configs/config.dart';


class AuthApi {
  Future<Object> register(login, password) async {
    var res = await dio.post(
        '/auth/register',
        data: {
          'login': login,
          'password': password
        }
    );
    return res;
  }

  Future<Object> login(login, password) async {
    var res = await http.post(
        configs['api']+'/auth/login',
        headers: {'Content-Type':'application/json'},
        body: jsonEncode({
          'login': login,
          'password': password
        })
    );
    return json.decode(res.body);
  }
}