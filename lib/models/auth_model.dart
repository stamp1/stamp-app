import 'package:stampapp/models/user_model.dart';

class AuthModel {
  final String user;
  final String token;

  AuthModel({this.user, this.token});

  factory AuthModel.fromJson(Map<String, dynamic> json) {
    return AuthModel(
      token: json['token'],
    );
  }
}