import 'package:stampapp/models/user_model.dart';

class Message {
  final String mType;
  final String content;
  final User sender;

  Message(this.mType, this.content, this.sender);
}