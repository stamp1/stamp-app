import 'message_model.dart';

class Dialog {
  final Map<String, dynamic> interlocutors;
  final Map<Message, dynamic> messages;

  Dialog({this.interlocutors, this.messages});

  factory Dialog.fromJson(Map<String, dynamic> json) {
    return Dialog(
      interlocutors: json['interlocutors'] as Map<String, dynamic>,
      messages: json['messages'] as Map<Message, dynamic>,
    );
  }
}