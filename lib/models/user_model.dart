class User {
  final int userId;
  final String userName;

  User({this.userId, this.userName});
}