import 'dart:convert';
import 'package:stampapp/api/auth_api.dart';
import 'package:stampapp/models/auth_model.dart';

class AuthController {
  final authApi = new AuthApi();

  register(login, password) async {
    return await authApi.register(login, password);
  }

  login(login, password) async {
    return await authApi.login(login, password);
  }
}