import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Square extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 45.0,
      height: 43.0,
      decoration: BoxDecoration(
          color: Color.fromRGBO(64, 223, 159, 1),
          borderRadius: BorderRadius.all(Radius.circular(12.0))
      ),
    );
  }
}
