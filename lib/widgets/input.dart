import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final String placeholder;
  final String type;
  final TextEditingController controller;

  Input(this.placeholder, this.type, this.controller);



  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 38.0,
          height: 48.0,
          margin: EdgeInsets.only(right: 20.0),
          decoration: BoxDecoration(
              color: getBackgroundColor(),
              borderRadius: BorderRadius.all(Radius.circular(12.0))
          ),
          child: Icon(
            getIcon(),
            color: getIconColor(),
            size: 24.0,
          ),
        ),
        Flexible(
          child: TextField(
            controller: controller,
            style: TextStyle(
                decorationColor: Colors.white,
                color: Colors.white
            ),
            decoration: InputDecoration(
              labelText: placeholder,
            ),
          ),
        ),
      ],
    );
  }

  IconData getIcon() {
    switch (type) {
      case 'login' :
        return Icons.person;
      case 'password':
        return Icons.lock;
    }
    return Icons.info;
  }

  getIconColor() {
    switch (type) {
      case 'login' :
        return Color.fromRGBO(255, 197, 66, 1);
      case 'password':
        return Color.fromRGBO(255, 87, 95, 1);
    }
  }

  getBackgroundColor() {
    switch (type) {
      case 'login' :
        return Color.fromRGBO(98, 91, 57, 1);
      case 'password':
        return Color.fromRGBO(98, 58, 66, 1);
    }
  }


}
