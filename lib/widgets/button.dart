import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String title;
  final String style;
  final Function callback;

  Button(this.title, this.callback, {this.style = 'green'});

  @override


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 58.0,
      child: this.style == 'green' ? RaisedButton(
        onPressed: () {
          this.callback(context);
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0)
        ),
        color: Color.fromRGBO(64, 223, 159, 1),
        child: Text(
            title,
            style: TextStyle(
                fontSize: 16,
                color: Colors.white
            )
        ),
      ) : this.style == 'transparent' ? RaisedButton(
        onPressed: () {
          this.callback(context);
        },
        color: Color.fromRGBO(255, 255, 255, 0.1),
        child: Text(
            title,
            style: TextStyle(
                fontSize: 16,
                color: Color.fromRGBO(150, 167, 175, 1)
            )
        ),
      ) : '',
    );
  }

  Color getBackgroundColor(buttonStyle) {
    Color color = Colors.white;
    switch (buttonStyle) {
      case 'green':
        color = Color.fromRGBO(64, 223, 159, 1);
        break;
    }
    return color;
  }
}
