import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class TitleDescription extends StatelessWidget {
  final String _title;

  TitleDescription(this._title);

  @override
  Widget build(BuildContext context) {
    return Text(
      _title,
      style: TextStyle(
          color: Color.fromRGBO(150, 167, 175, 100.0),
          fontSize: 24.0
      ),
    );
  }
}
