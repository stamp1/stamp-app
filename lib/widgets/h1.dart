import 'package:flutter/material.dart';

class H1 extends StatelessWidget {
  final String _title;

  H1(this._title);

  @override
  Widget build(BuildContext context) {
    return Text(
        _title,
      style: TextStyle(
        color: Colors.white,
        fontSize: 48.0
      ),
    );
  }
}
