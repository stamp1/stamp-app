import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:stampapp/controllers/auth_controller.dart';
import 'package:stampapp/screens/dashboard/dashboard.dart';
import 'package:stampapp/screens/news/news.dart';
import 'package:stampapp/widgets/button.dart';
import 'package:stampapp/widgets/h1.dart';
import 'package:stampapp/widgets/input.dart';
import 'package:stampapp/widgets/square.dart';
import 'package:stampapp/widgets/title_description.dart';
import 'dart:convert';

class LoginTab extends StatefulWidget {
  final Function changeTab;

  const LoginTab ({ Key key, this.changeTab }): super(key: key);

  @override
  _LoginTabState createState() => _LoginTabState();
}

class _LoginTabState extends State<LoginTab> {
  final loginController = TextEditingController();
  final passwordController = TextEditingController();


  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    loginController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(31, 46, 53, 100.0),
      body: Container(
        alignment: Alignment.bottomCenter,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Square(),
                SizedBox(height: 25),
                H1('Login'),
                SizedBox(height: 10),
                TitleDescription('Sign to continue'),
                SizedBox(height: 35),
                Input('Login', 'login', loginController),
                SizedBox(height: 35),
                Input('Password', 'password', passwordController),
                SizedBox(height: 35),
                Button('Log in', logIn),
                SizedBox(height: 35),
                Button('Create an account', widget.changeTab, style: 'transparent'),
                // changeTab
              ],
            ),
          ),
        ),
      ),
    );
  }

  logIn(context) async {
    final authController = new AuthController();
    final auth = await authController.login(loginController.text, passwordController.text);
    debugPrint(json.encode(auth));
    if ( auth['status'] == 200 ) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => News()),
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Some error"),
            content: new Text(auth["message"]),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }
}
