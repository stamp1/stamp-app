import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:stampapp/screens/auth/tabs/login.dart';
import 'package:stampapp/screens/auth/tabs/sigup.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {

  bool login = true;

  @override
  Widget build(BuildContext context) {
    return login ? LoginTab(changeTab: changeTabs) : SignUpTab(changeTab: changeTabs);
  }

  changeTabs(context) {
    setState(() {
      login = !login;
    });
  }
}


//AuthScreen
