import 'package:flutter/material.dart';
import 'package:stampapp/screens/auth/auth.dart';
import 'package:stampapp/widgets/button.dart';
import 'package:stampapp/widgets/h1.dart';
import 'package:stampapp/widgets/square.dart';
import 'package:stampapp/widgets/title_description.dart';

class WelcomeScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(31, 46, 53, 100.0),
      body: Container(
        alignment: Alignment.bottomCenter,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Square(),
                SizedBox(height: 25),
                H1('Welcome!'),
                SizedBox(height: 10),
                TitleDescription('Sign to continue'),
                SizedBox(height: 35),
                Button('Sign in', toSignInScreen),
                SizedBox(height: 35),
              ],
            ),
          ),
        ),
      ),
    );
  }

  toSignInScreen(context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AuthScreen()),
    );
  }
}
