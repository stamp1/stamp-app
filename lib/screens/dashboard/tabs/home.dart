import 'package:flutter/material.dart';
import 'package:stampapp/widgets/h1.dart';
import 'package:stampapp/widgets/square.dart';

class HomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return H1('Home!');
  }
}
