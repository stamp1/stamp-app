import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:stampapp/screens/welcome.dart';
import 'package:stampapp/screens/dashboard/dashboard.dart';
import 'screens/auth/auth.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Future<void> initState() async {
    super.initState();

    final storage = new FlutterSecureStorage();

    Map<String, String> allValues = await storage.readAll();

    print(allValues);

    await storage.write(key: 'test', value: 'foobar');
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stamp MVP',
      theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
          primaryColor: Colors.white,
          accentColor: Colors.orange,
          hintColor: Color.fromRGBO(150, 167, 175, 1)
      ),
      initialRoute: '/',
      routes: {
        '/' : (context) => WelcomeScreen(),
        '/signin' : (context) => AuthScreen(),
        '/dashboard' : (context) => DashboardScreen(),
      },
    );
  }
}